﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ConverterH264toMp4
{
    /*
     * Thanks for: https://stackoverflow.com/a/3481274 
     */
    public class WaitCursor : IDisposable
    {
        private Cursor _previousCursor;

        public WaitCursor()
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                _previousCursor = Mouse.OverrideCursor;

                Mouse.OverrideCursor = Cursors.Wait;
            }));
        }

        #region IDisposable Members

        public void Dispose()
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                Mouse.OverrideCursor = _previousCursor;
            }));
        }

        #endregion
    }
}
