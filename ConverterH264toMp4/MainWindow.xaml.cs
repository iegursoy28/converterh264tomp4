﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Win32;
using Renci.SshNet;

namespace ConverterH264toMp4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ContextMenu ContextMenuRemoteListItem = new ContextMenu()
        {
            Name = "ContextMenuRemoteListItem",
            Items = { new MenuItem() { Header = "Download", Tag = "download" } }
        };

        private ContextMenu ContextMenuLocalListItem = new ContextMenu()
        {
            Name = "ContextMenuLocalListItem",
            Items = {
                new MenuItem() { Header = "Refresh", Tag = "refresh" },
                new MenuItem() { Header = "Convert", Tag = "convert" }
            }
        };

        private ContextMenu ContextMenuLocalList = new ContextMenu()
        {
            Name = "ContextMenuLocalListItem",
            Items = {
                new MenuItem() { Header = "Refresh", Tag = "refresh" }
            }
        };

        private readonly Brush TextForeground = Brushes.WhiteSmoke;

        public string Host { get; set; } = "10.5.176.240";
        public int Port { get; set; } = 22;
        public string User { get; set; } = "root";
        public string Pass { get; set; } = "qwe";
        public string RemotePath { get; set; } = "/mnt/records/";
        public string LocalPath { get; set; } = $"{Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}\\files";

        public MainWindow()
        {
            if (!Directory.Exists(LocalPath))
                Directory.CreateDirectory(LocalPath);

            InitializeComponent();

            txtUser.Text = User;
            txtUser.TextChanged += TxtUser_TextChanged;

            txtHost.Text = Host;
            txtHost.TextChanged += TxtHost_TextChanged;

            txtPass.Password = Pass;
            txtPass.PasswordChanged += TxtPass_PasswordChanged;

            txtRemotePath.IsEnabled = true;
            txtRemotePath.Text = RemotePath;
            txtRemotePath.TextChanged += TxtRemotePath_TextChanged;

            txtLocalPath.IsEnabled = true;
            txtLocalPath.Text = LocalPath;
            txtLocalPath.TextChanged += TxtLocalPath_TextChanged;

            foreach (MenuItem item in ContextMenuRemoteListItem.Items)
                item.Click += RemoteMenuItem_Click;
            foreach (MenuItem item in ContextMenuLocalListItem.Items)
                item.Click += LocalMenuItem_Click;
            foreach (MenuItem item in ContextMenuLocalList.Items)
                item.Click += LocalMenuList_Click;

            lstLocal.ContextMenu = ContextMenuLocalList;

            RefreshLocalList();
        }

        private void LocalMenuList_Click(object sender, RoutedEventArgs e)
        {

            if (sender is MenuItem)
            {
                var item = sender as MenuItem;
                var tag = item.Tag;

                if (tag == null) return;

                if (tag.Equals("refresh"))
                {
                    RefreshLocalList();
                }
            }
        }

        private void LocalMenuItem_Click(object sender, RoutedEventArgs e)
        {

            if (sender is MenuItem)
            {
                var item = sender as MenuItem;
                var tag = item.Tag;

                if (tag == null || !(this.lstLocal.SelectedValue is TextBlock)) return;

                if (tag.Equals("refresh"))
                {
                    RefreshLocalList();
                }
                else if (tag.Equals("convert"))
                {
                    var fileItem = this.lstLocal.SelectedValue as TextBlock;
                    var file = fileItem.Text;
                    Console.WriteLine($"Convert file: {file}");
                    Convert2Mp4(file);
                    RefreshLocalList();
                }
            }
        }

        private bool isDowloadFile = false;
        private void RemoteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (isDowloadFile) return;

            if (sender is MenuItem)
            {
                var item = sender as MenuItem;
                var tag = item.Tag;

                if (tag == null || !(this.lstRemote.SelectedValue is TextBlock)) return;

                if (tag.Equals("download"))
                {
                    var fileItem = this.lstRemote.SelectedValue as TextBlock;
                    var file = fileItem.Text;
                    Console.WriteLine($"Download file: {file}");

                    new System.Threading.Thread(() =>
                    {
                        isDowloadFile = true;
                        using (new WaitCursor())
                        using (var client = new ScpClient(Host, Port, User, Pass))
                        {
                            try
                            {
                                client.Connect();
                                client.Downloading += Client_Downloading;
                                client.Download($"{RemotePath}/{file}", new DirectoryInfo(LocalPath));
                            }
                            catch (Exception err)
                            {
                                Console.WriteLine(err.Message);
                            }
                        }
                        isDowloadFile = false;
                        RefreshLocalList();
                    }).Start();
                }
            }
        }

        long tick = 0;

        private void Client_Downloading(object sender, Renci.SshNet.Common.ScpDownloadEventArgs e)
        {
            if (DateTime.UtcNow.Ticks - tick > TimeSpan.TicksPerSecond)
            {
                Console.WriteLine($"[{DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff")}] Dowloading {e.Filename} %{ ((float)e.Downloaded / (float)e.Size) * 100}");
                tick = DateTime.UtcNow.Ticks;
            }
        }

        private void TxtLocalPath_TextChanged(object sender, TextChangedEventArgs e)
        {
            LocalPath = this.txtLocalPath.Text;
        }

        private void TxtRemotePath_TextChanged(object sender, TextChangedEventArgs e)
        {
            RemotePath = this.txtRemotePath.Text;
        }

        private void TxtPass_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Pass = this.txtPass.Password;
        }

        private void TxtHost_TextChanged(object sender, TextChangedEventArgs e)
        {
            Host = this.txtHost.Text;
        }

        private void TxtUser_TextChanged(object sender, TextChangedEventArgs e)
        {
            User = this.txtUser.Text;
        }

        private bool isRunCmd = false;
        private void Connect_Button_Click(object sender, RoutedEventArgs e)
        {
            if (isRunCmd) return;
            Console.WriteLine($"ssh {User}@{Host}");
            new System.Threading.Thread(() =>
            {
                isRunCmd = true;
                using(new WaitCursor())
                using (var client = new SshClient(Host, Port, User, Pass))
                {
                    try
                    {
                        client.Connect();
                        var cmd = client.RunCommand($"ls -1 -p {RemotePath} | grep -v /");
                        var resp = cmd.Execute();

                        lstRemote.Dispatcher.Invoke((Action)(() =>
                        {
                            lstRemote.Items.Clear();
                            var list = resp.Split('\n');
                            foreach (var item in list)
                            {
                                if (item.Trim().Length <= 0)
                                    continue;

                                var i = new TextBlock();
                                i.Foreground = TextForeground;
                                i.Text = item;
                                i.Tag = item;
                                i.ContextMenu = this.ContextMenuRemoteListItem;
                                lstRemote.Items.Add(i);
                            }
                        }));
                    }
                    catch (Exception err)
                    {
                        Console.WriteLine(err.Message);
                    }
                }
                isRunCmd = false;
            }).Start();
        }

        private void RefreshLocalList()
        {
            lstLocal.Dispatcher.Invoke((Action)(() =>
            {
                lstLocal.Items.Clear();
                var files = Directory.GetFiles(LocalPath);

                foreach (var item in files)
                {
                    if (item.Trim().Length <= 0)
                        continue;

                    var i = new TextBlock();
                    i.Foreground = TextForeground;
                    i.Text = item;
                    i.Tag = item;
                    i.ContextMenu = this.ContextMenuLocalListItem;
                    lstLocal.Items.Add(i);
                }
            }));
        }

        private void Convert2Mp4(string file)
        {
            try
            {
                FileInfo info = new FileInfo(file);

                var output = $"{LocalPath}\\{info.Name}_{DateTime.Now.Ticks}.mp4";

                ProcessStartInfo startInfo = new ProcessStartInfo()
                {
                    FileName = $"{Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}\\ffmpeg\\ffmpeg.exe",
                    Arguments = $"-i {file} -vcodec copy {output}",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                };

                new System.Threading.Thread(() =>
                {
                    using (new WaitCursor())
                    {
                        Console.WriteLine("start");
                        Process proc = new Process() { StartInfo = startInfo, };
                        proc.Start();

                        while (!proc.StandardOutput.EndOfStream)
                        {
                            Console.WriteLine(proc.StandardOutput.ReadLine());
                        }

                        if (!proc.HasExited)
                            proc.WaitForExit(4000);

                        Console.WriteLine("exit");
                    }
                }).Start();
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }
    }
}
